#ifndef MEMORY_VARIABLES_HPP
#define MEMORY_VARIABLES_HPP

/*--------------------------------- Page One ---------------------------------*/

// Variables for Communications
#define ROUTER_ADDRESS 0x00
#define MACRO_TYPE 0x01
#define MACRO_DATA 0x02
#define REQUEST 0x03
#define CONNECTED 0x04
#define CONNECTED2 0x05 // used by the touch screen
#define MACRO_IN_PROGRESS 0x06
#define TIMEOUT_IN_PROGRESS 0x07
#define TRANSMIT_MACRO 0x08

/*--------------------------------- Page Two ---------------------------------*/

// Variables for Inputs
#define JOYSTICK_LEFT_X  0x20
#define JOYSTICK_LEFT_Y 0x21
#define JOYSTICK_RIGHT_X 0x22
#define JOYSTICK_RIGHT_Y 0x23
#define SLIDER_LEFT 0x24
#define SLIDER_RIGHT 0x25

/*-------------------------------- Page Three --------------------------------*/



/*-------------------------------- Page Four ---------------------------------*/


#endif // MEMORY_VARIABLES_HPP
