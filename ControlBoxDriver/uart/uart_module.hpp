#ifndef UART_MODULE_HPP
#define UART_MODULE_HPP

typedef enum
{
  UART_0,
  UART_1,
  UART_2,
  UART_3
} UART_MODULE;

#endif // UART_MODULE_HPP